<?php

header("Access-Control-Allow-Origin: *");
header("Content-type: application/json; charset=utf-8");

require_once "connection.php";

$params = json_decode(file_get_contents('php://input'),true);

$Title = $params['Title'];
$Desc = $params['Desc'];
$Date = date("Y-m-d H:i:s");

$query = $PDO->prepare("INSERT INTO `projects` VALUES (NULL, '$Title', '$Desc', '$Date', '$Date', 0)");
$state = $query->execute();

echo json_encode(["code" => (int) !$state]);