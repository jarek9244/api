<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-type: application/json; charset=utf-8");

    require_once "connection.php";

    $params = json_decode(file_get_contents('php://input'),true);
    $ID = $params['id'];
    $status = [];
    
    $query = $PDO->prepare("SELECT `ProjectID`, `Done` FROM tasks WHERE ID = :id");
    $query->bindValue(":id", $ID, PDO::PARAM_INT);
    $query->execute();

    $status = $query->fetch(PDO::FETCH_ASSOC);
    $status['Done'] = (int) $status['Done'];
    $status['ProjectID'] = (int) $status['ProjectID'];

    $newVal = ($status['Done'] + 1) % 2;

    $query = $PDO->prepare("UPDATE Tasks SET `Done` = :val WHERE ID = :id");
    $query->bindValue(":val", $newVal, PDO::PARAM_INT);
    $query->bindValue(":id", $ID, PDO::PARAM_INT);
    $complete = $query->execute();
    
    $query = $PDO->prepare("SELECT COUNT(ID) as Amount, SUM(DONE) as Checked FROM tasks WHERE `ProjectID` = :id");
    $query->bindValue(":id", $status['ProjectID'], PDO::PARAM_INT);
    $query->execute();

    $result = $query->fetch(PDO::FETCH_ASSOC);
    $result['Amount'] = (int) $result['Amount'];
    $result['Checked'] = (int) $result['Checked'];

    $perc = 0;
    if($result['Amount'] > 0) {
        $perc = round(($result['Checked'] / $result['Amount']) * 100);
    }
    
    echo json_encode(["code" => (int) !$complete, "value" => (int) $newVal, "perc" => $perc]);