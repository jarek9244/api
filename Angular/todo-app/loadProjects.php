<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-type: application/json; charset=utf-8");

    require_once "connection.php";

    $projects = [];
    $query = $PDO->prepare("SELECT * FROM projects WHERE Done = 0");
    $query->execute();

    while($row = $query->fetch(PDO::FETCH_ASSOC)) {
        $row["ID"] = (int) $row["ID"];
        $projects[] = $row;
    }

    echo json_encode($projects);