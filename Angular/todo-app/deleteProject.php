<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-type: application/json; charset=utf-8");

    require_once "connection.php";

    $params = json_decode(file_get_contents('php://input'),true);
    $ID = (int) $params['ID'];

    $query = $PDO->prepare("DELETE FROM projects WHERE `ID` = :id");
    $query->bindValue(":id", $ID, PDO::PARAM_INT);
    $complete = $query->execute();
    
    echo json_encode($complete);