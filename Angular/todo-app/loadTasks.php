<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-type: application/json; charset=utf-8");
    $projectID = $_GET['id'];

    require_once "connection.php";
    $projects = [];
    $query = $PDO->prepare("SELECT * FROM tasks WHERE ProjectID = :id");
    $query->bindParam(":id", $projectID, PDO::PARAM_INT);
    $query->execute();

    while($row = $query->fetch(PDO::FETCH_ASSOC)) {
        $row["ID"] = (int) $row["ID"];
        $projects[] = $row;
    }

    $mainTasks = [];

    foreach ($projects as $proj) {
        $proj["ID"] = (int) $proj["ID"];
        $proj["ProjectID"] = (int) $proj["ProjectID"];
        $proj["TaskID"] = (int) $proj["TaskID"];
        $proj["Done"] = (int) $proj["Done"];

        if($proj["TaskID"] == 0) {
            $mainTasks[$proj["ID"]] = $proj;
            $mainTasks[$proj["ID"]]["Subtasks"] = [];
        } else {
            $mainTasks[$proj["TaskID"]]["Subtasks"][] = $proj;
        }
    }
    
    echo json_encode($mainTasks);