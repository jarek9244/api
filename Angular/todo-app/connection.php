<?php 

    $conn = require "config.php";

    $PDO = null;

    try {
        $PDO = new PDO("mysql:host={$conn['db_host']};dbname={$conn['db_base']}; charset=utf8;", $conn["db_user"], $conn["db_password"]);
    } catch(PDOException $e) {
        echo $e->getMessage();
    }