<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-type: application/json; charset=utf-8");

    require_once "connection.php";

    $params = json_decode(file_get_contents('php://input'),true);
    $PID = $params['PID'];
    $TID = $params['TID'];
    $text = $params['data'];
    $created = date('Y-m-d H:i:s');
    
    $query = $PDO->prepare("INSERT INTO tasks VALUES (null, :PID, :TID, :Title, :Created, 0)");
    $query->bindValue(":PID", $PID, PDO::PARAM_INT);
    $query->bindValue(":TID", $TID, PDO::PARAM_INT);
    $query->bindValue(":Title", $text, PDO::PARAM_STR);
    $query->bindValue(":Created", $created, PDO::PARAM_STR);
    $complete = $query->execute();

    $lastID = (int) $PDO->lastInsertId();
    
    $query = $PDO->prepare("SELECT COUNT(ID) as Amount, SUM(DONE) as Checked FROM tasks WHERE `ProjectID` = :id");
    $query->bindValue(":id", $PID, PDO::PARAM_INT);
    $query->execute();

    $result = $query->fetch(PDO::FETCH_ASSOC);
    $result['Amount'] = (int) $result['Amount'];
    $result['Checked'] = (int) $result['Checked'];

    $perc = 0;
    if($result['Amount'] > 0) {
        $perc = round(($result['Checked'] / $result['Amount']) * 100);
    }
    
    echo json_encode(["info" => (int) !$complete, "task" => [
        'ID' => $lastID,
        'ProjectID' => $PID,
        'TaskID' => $TID,
        'Title' => $text,
        'Crated' => $created,
        'Done' => 0
    ]]);