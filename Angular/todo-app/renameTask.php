<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-type: application/json; charset=utf-8");

    require_once "connection.php";

    $params = json_decode(file_get_contents('php://input'),true);
    $ID = $params['ID'];
    $text = $params['data'];
 
    $query = $PDO->prepare("UPDATE Tasks SET `Title` = :txt WHERE ID = :id");
    $query->bindValue(":txt", $text, PDO::PARAM_STR);
    $query->bindValue(":id", $ID, PDO::PARAM_INT);
    $complete = $query->execute();
    
    echo json_encode(['code' => (int) !$complete, 'txt' => $text]);